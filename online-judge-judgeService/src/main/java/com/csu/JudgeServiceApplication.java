package com.csu;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * 代码沙箱judgeService启动类
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 10:49
 */
@SpringBootApplication
@EnableDiscoveryClient
public class JudgeServiceApplication{
    public static void main(String[] args) {
        SpringApplication.run(JudgeServiceApplication.class, args);
    }
}