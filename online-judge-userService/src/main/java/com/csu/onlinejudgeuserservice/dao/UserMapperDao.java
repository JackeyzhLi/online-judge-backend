package com.csu.onlinejudgeuserservice.dao;

import com.csu.onlinejudgeuserservice.model.entity.UserEntity;
import com.csu.onlinejudgeuserservice.model.vo.UserSearchParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapperDao {

    List<UserEntity> queryAllUserNames(@Param("userSearchParam") UserSearchParam userSearchParam);

}