package com.csu.onlinejudgeuserservice.service.impl;

import com.csu.onlinejudgeuserservice.dao.UserMapperDao;
import com.csu.onlinejudgeuserservice.model.entity.UserEntity;
import com.csu.onlinejudgeuserservice.model.vo.UserSearchParam;
import com.csu.onlinejudgeuserservice.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户服务Service
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 21:43
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapperDao userMapperDao;

    @Override
    public PageInfo<UserEntity> queryUserTest(UserSearchParam userSearchParam,
                                              int pageNum, int pageSize) {


        PageHelper.startPage(pageNum, pageSize);
        List<UserEntity> userNames = userMapperDao.queryAllUserNames(userSearchParam);

        return new PageInfo<>(userNames);
    }
}
