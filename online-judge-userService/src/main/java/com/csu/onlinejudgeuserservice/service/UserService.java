package com.csu.onlinejudgeuserservice.service;

import com.csu.onlinejudgeuserservice.model.entity.UserEntity;
import com.csu.onlinejudgeuserservice.model.vo.UserSearchParam;
import com.github.pagehelper.PageInfo;

/**
 * 用户服务Service
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 21:42
 */
public interface UserService {

    /**
     * 获取所有用户
     *
     * @return String
     */
    PageInfo<UserEntity> queryUserTest(UserSearchParam userSearchParam, int pageNum, int pageSize);
}
