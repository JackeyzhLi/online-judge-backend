package com.csu.onlinejudgeuserservice.controller;

import com.alibaba.cloud.context.utils.StringUtils;
import com.csu.common.model.BaseResponse;
import com.csu.common.util.CheckUtils;
import com.csu.onlinejudgeuserservice.model.entity.UserEntity;
import com.csu.onlinejudgeuserservice.model.vo.UserSearchParam;
import com.csu.onlinejudgeuserservice.service.UserService;
import com.github.pagehelper.PageInfo;
import io.netty.util.internal.StringUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户服务Controller
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 21:40
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/test/{pageNum}/{pageSize}")
    public PageInfo<UserEntity> queryUserTest(@RequestBody UserSearchParam userSearchParam,
                                              @PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize) {
        PageInfo<UserEntity> userEntityPageInfo = userService.queryUserTest(userSearchParam, pageNum, pageSize);


        return userEntityPageInfo;
    }

    @PostMapping("/test2")
    public BaseResponse queryBaseResponse(@RequestParam("msg") String msg) {
        CheckUtils.check(StringUtils.isNotEmpty(msg), "msg不能为空");
        return BaseResponse.success(msg);
    }
}
