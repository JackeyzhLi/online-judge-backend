package com.csu.onlinejudgeuserservice.model.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户搜索参数
 *
 * @author: lizhenhuan
 * @date: 2024/5/6 14:18
 */
@Getter
@Setter
public class UserSearchParam {
    private String userAccount;

    private String userName;

}
