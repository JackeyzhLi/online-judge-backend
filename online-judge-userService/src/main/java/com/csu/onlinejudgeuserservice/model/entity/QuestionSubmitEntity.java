package com.csu.onlinejudgeuserservice.model.entity;

import com.csu.common.model.entity.BaseDataEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 题目提交实体类
 *
 * @author: lizhenhuan
 * @date: 2024/5/6 16:02
 */
@Getter
@Setter
public class QuestionSubmitEntity extends BaseDataEntity {
    private Long questionId;

    private String language;

    private Byte judgeStatus;
}