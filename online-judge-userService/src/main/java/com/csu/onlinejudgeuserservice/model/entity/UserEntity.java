package com.csu.onlinejudgeuserservice.model.entity;

import com.csu.common.model.entity.BaseDataEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户实体类
 *
 * @author: lizhenhuan
 * @date: 2024/5/6 16:02
 */
@Getter
@Setter
public class UserEntity extends BaseDataEntity {
    private String userAccount;

    private String userPassword;

    private String unionId;

    private String mpOpenId;

    private String userName;

    private String userAvatar;

    private String userProfile;

    private String userRole;

}