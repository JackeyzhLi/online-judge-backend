package com.csu.onlinejudgeuserservice.model.entity;

import com.csu.common.model.entity.BaseDataEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 题目实体类
 *
 * @author: lizhenhuan
 * @date: 2024/5/6 16:02
 */
@Getter
@Setter
public class QuestionEntity extends BaseDataEntity {
    /**
     * 标题
     */
    private String title;

    /**
     * 标签
     */
    private String tags;

    /**
     *
     */
    private Long accept;

    /**
     * 提交数
     */
    private Long submit;
}