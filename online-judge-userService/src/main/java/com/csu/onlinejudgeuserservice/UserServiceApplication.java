package com.csu.onlinejudgeuserservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 用户服务启动类
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 10:49
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.csu.onlinejudgeuserservice.dao")
public class UserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}
