package com.csu.onlinejudgeuserservice.exception.handler;

import com.csu.common.exception.BusinessException;
import com.csu.common.model.BaseResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理器
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 16:12
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    // 处理自定义的业务异常
     @ExceptionHandler(BusinessException.class)
     @ResponseBody
     public BaseResponse businessExceptionHandler(BusinessException e) {
         return BaseResponse.fail(e.getMessage());
     }
}
