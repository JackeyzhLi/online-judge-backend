package com.csu.onlinejudgegateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TestController
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 13:53
 */
@RestController
@RequestMapping("/gatewayTestController")
public class GatewayTestController {
    @Value("${adasd}")
    private String testValue;

    @PostMapping("/test01")
    public String getConfig() {
        return testValue;
    }
}
