package com.csu.onlinejudgegateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 在线判题网关服务
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 14:24
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class OnlineJudgeGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineJudgeGatewayApplication.class, args);
    }

}
