package com.csu.common.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 程序语言枚举
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 15:42
 */
@Getter
@AllArgsConstructor
public enum CodeLanguageEnum {
    JAVA(0,"Java"),

    CPP(1, "C++"),

    PYTHON(2, "Python");

    private final int code;

    private final String desc;
}

