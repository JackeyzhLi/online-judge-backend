package com.csu.common.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户Dto
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 22:11
 */
@Getter
@Setter
public class UserDto {
    /**
     * 主键id
     */
    private long id;

    /**
     * 用户名称
     */
    private String userName;
}
