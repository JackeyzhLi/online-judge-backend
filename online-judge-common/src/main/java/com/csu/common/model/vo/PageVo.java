package com.csu.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 分页Vo
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 22:14
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PageVo {
    /**
     * 当前页
     */
    private int current;

    /**
     * 分页大小
     */
    private int pageSize;

    /**
     * 查询总数
     */
    private int total;

    public PageVo(int current, int pageSize) {
        this.current = current;
        this.pageSize = pageSize;
    }
}
