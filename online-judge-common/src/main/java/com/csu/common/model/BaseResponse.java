package com.csu.common.model;

import com.csu.common.consts.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 基础响应类
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 15:12
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse<T> implements Serializable {

    private int code;

    private T data;

    private String message;

    public BaseResponse(int code, T data) {
        this(code, data, "");
    }

    public BaseResponse(ResponseCode responseCode) {
        this(responseCode.getCode(), null, responseCode.getMessage());
    }

    public BaseResponse(ResponseCode responseCode, T data) {
        this(responseCode);
        this.data = data;
    }

    public static <K> BaseResponse<K> success(K data) {
        BaseResponse<K> baseResponse = new BaseResponse<>();
        baseResponse.setCode(ResponseCode.SUCCESS.getCode());
        baseResponse.setMessage(ResponseCode.SUCCESS.getMessage());
        baseResponse.setData(data);
        return baseResponse;
    }

    public static <K> BaseResponse<K> fail(String message) {
        BaseResponse<K> baseResponse = new BaseResponse<>();
        baseResponse.setCode(ResponseCode.ERROR.getCode());
        baseResponse.setMessage(message);
        return baseResponse;
    }
}
