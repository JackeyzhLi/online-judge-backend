package com.csu.common.model;

/**
 * 分页请求
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 15:14
 */
public class PageRequest {

    /**
     * 当前页号
     */
    private final int current = 1;

    /**
     * 页面大小
     */
    private final int pageSize = 10;
}
