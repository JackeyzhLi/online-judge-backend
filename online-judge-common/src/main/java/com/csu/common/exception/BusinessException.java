package com.csu.common.exception;

import com.csu.common.consts.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 业务异常
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 16:16
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BusinessException extends RuntimeException{
    /**
     * code
     */
    private int code;

    /**
     * message
     */
    private String message;

    public BusinessException(ResponseCode responseCode) {
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
    }
}
