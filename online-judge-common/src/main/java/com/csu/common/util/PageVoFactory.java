package com.csu.common.util;

import com.csu.common.model.vo.PageVo;

/**
 * 分页Vo工厂
 *
 * @author: lizhenhuan
 * @date: 2024/5/5 22:13
 */
public class PageVoFactory {

    /**
     * 分页大小限制
     */
    private static final int PAGE_SIZE_LIMIT = 200;

    public static PageVo getPageVo(int current, int pageSize) {
        if (current < 0 || pageSize < 0 || pageSize >= PAGE_SIZE_LIMIT) {
            // todo 统一异常处理，参数不对就抛出异常
            return null;
        }
        return new PageVo(current, pageSize);
    }
}
