package com.csu.common.util;

import com.csu.common.consts.ResponseCode;
import com.csu.common.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import sun.swing.StringUIClientPropertyKey;

import java.util.Locale;

/**
 * 参数校验工具类
 *
 * @author: lizhenhuan
 * @date: 2024/5/12 16:18
 */
public class CheckUtils {
    /**
     * 判断condition是否为true，如果不为true则抛出异常
     *
     * @param condition condition
     */
    public static void check(boolean condition){
        if (!condition) {
            throw new BusinessException(ResponseCode.ERROR);
        }
    }

    /**
     * 判断condition是否为true，如果不为true则抛出异常
     *
     * @param condition 条件
     * @param message 提示信息
     * @param msg 提示信息参数
     */
    public static void check(boolean condition, String message, String... msg){
        if (!condition) {
            throw new BusinessException(ResponseCode.ERROR.getCode(), String.format(message, msg, Locale.ROOT));
        }
    }

    /**
     * 判断str是否为empty，如果为empty则抛出异常
     *
     * @param str str
     * @param msg 提示信息
     */
    public static void checkNotEmpty(String str, String msg) {
        CheckUtils.check(StringUtils.isNotEmpty(str), msg);
    }

    /**
     * 判断str是否为blank，如果为empty则抛出异常
     *
     * @param str str
     * @param msg 提示信息
     */
    public static void checkNotBlank(String str, String msg) {
        CheckUtils.check(StringUtils.isNotBlank(str), msg);
    }
}
